---
title: "Getting Started With GTK"
date: 2020-06-29T21:51:18+01:00
draft: false 
---

For quite a while I've wanted to learn how to write a desktop application using
[GTK](https://gtk.org). I've finally stopped procrastinating and started
teaching myself how to do so. The result:

{{< figure src="/images/sums.png" caption="Redesign (maybe) pending" >}}

Sums is a simple postfix calculator that I've been intermittently hacking on
over the past few weeks. Although I fully intend for it to become a useful
application in its own right, it's primarily my vehicle for getting to know
GTK and GUI programming in general. I also want to publish it on
[Flathub](https://flathub.org) so I can get familiar with that process and give
something small back to the free software community.

Currently, its functionality is extremely limited and it can't deal with
malformed expressions at all. It's also, sadly, icon-less.

Sums is written in C and developed for [GNOME](https://gnome.org) + Linux. GTK
has several bindings that let programmers write applications in "easier"
languages like Python or JavaScript, but I really enjoy writing C.

You can find the source code [here](https://gitlab.com/leesonwai/sums): it's
free software under the GPL version 3.
