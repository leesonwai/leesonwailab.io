---
title: "Hugo and Inkscape: a reflection"
date: 2019-11-27T11:08:44Z
series: "IT Professional Skills"
draft: false
---

_This is a software review for my 'IT Professional Skills' module_

Over the past couple of weeks, I've been writing and maintaing a four-part blog
series as a college assignment. If you haven't read those posts and may be
interested in a basic overview of binary trees , I highly recommend you go back
and read them. This entry is an addendum of sorts--a reflection on two pieces of
software I've been using to complete my work: Hugo and Inkscape. I'll be
providing a brief outline of my experiences using them.

## Hugo
This blog is running on [Hugo](https://gohugo.io/)--a free software static site
generator written in Google's Go programming language--and Gitlab Pages. Pages
is a Gitlab feature that allows users to publish static websites simply by
pushing files to a `git` repository. Hugo is just one of several static site
generators that you can use, but I confess to choosing it for no reason in
particular. Another popular static site generator is Jekyll, a Ruby-based
project, but I can't comment on how it compares.

With my Fedora installation, installing Hugo is as easy as:

```none
$ sudo dnf install hugo
```

Hugo's community provides a plethora of different themes to style your website
with--some focused on providing a portfolio, others on blogging and so on. Blog
entries are written in Markdown and Hugo takes care of organising your website
and generating the HTML.

I appreciate Hugo's simple `git`-based workflow. Adding a new blog entry takes
just a few commands and within seconds, the entry appears on my website:

```none
$ hugo new posts/a-blog-entry.md
$ git add content/posts/a-blog-entry.md
$ git commit -m "posts: Add new blog entry"
$ git push
```

The Markdown files can be edited in a simple text editor and each post can be
previewed locally by running `hugo server -D`, so you can ensure they look as
you want them to before publishing them. If you find that Markdown is not
expressive enough for certain elements you want to create, you can embed HTML
instead.

However, this type of workflow is not to everyone's tastes. The most-used
platform amongst my peers seems to have been Weebly, which--as far as I've
seen--is point-and-click and browser-based.

## Inkscape
Another area that the module has focused on is creating application mock-ups.
I appreciate the importance of design-lead development, thus creating mock-ups
is a skill I want to develop. As always, my preference is for using free
software so [Inkscape](https://inkscape.org/) was the natural choice.

Inkscape is a free software vector graphics editor. It can be used for various
tasks, from illustration and sketches to icon design and mock-ups. Like most
large free software projects, Inkscape has an active community of contributors
that provide many tutorials to help new users learn how to navigate its UI and
use its tools to the greatest effect.

{{< figure src="/images/inkscape.png" caption="Inkscape in action" >}}

Still, like most complex applications, it takes a while to get to grips with
Inkscape's capabilities and how to use them. I've used Inkscape in another
module to create an application mock-up, but I still wouldn't consider myself
proficient in it.

For readers who may be confused by the use of the term "free software" and
understandably believe it is a reference to price--that isn't so. When I use
that term, I'm referring to software distrubuted under a free license, as
opposed to software distributed under a proprietary license, such as Adobe
Photoshop.

I believe it's important not to value just features and/or convenience, but
also whether the software you use is free or proprietary. Considering this,
Inkscape offers something online mock-up tools or Adobe's products don't, and
that's the freedom to use, share and modify the software you use as you see fit.
For non-programmers, this may not seem like an important distinction, but I
recommend you bear it in mind when choosing what software you use.

Inkscape has served me well in the brief time I've been using it and I encourage
you to check it out. Not only is it free, but it doesn't cost a cent to download
and install.
