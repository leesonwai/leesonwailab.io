---
title: "Sum updates"
date: 2021-04-02T16:11:47+01:00
draft: false
---

It's been a long time since I wrote about Sums, the small GTK application I
began working on last year. In the period since, development has been
progressing steadily and I've learned a lot about GTK and GObject.

I wish now I'd organised and recorded my experiences while developing the app
and navigating the GTK/GLib/GObject documentation. My experience was positive;
the GNOME documentation is generally fine, although I found myself reading the
source code of other C GTK applications to get an understanding of a several
development patterns.

The app has undergone a couple of facelifts and a port to GTK 4, as well as
adding a host of new features and niceties. I recycled an old GNOME Calculator
icon and I've tried to follow the GNOME HIG and latest design patterns wherever
I can. I think it at least _feels_ like a "real" app now, even if its feature
set will likely always remain limited.

{{< figure src="/images/sums-0.7.png" >}}

I'm happy to say I achieved my original goal of publishing it on Flathub.
[Try it out](https://flathub.org/apps/details/io.gitlab.leesonwai.Sums) and tell
me how many features it's lacking!

## The future

Time, interest and ability willing, there's still a few things I'd like to work
on. I've taught Sums to do as-you-type operator-to-notation substitution and I'd
like to hook up an animation to this process, if it's possible. Builder has a
nice effect for autocompletion, but I don't know how that's accomplished. I'd
also like to autohide or flatten the header bar to create an "all content" feel
for the app, similar to [Solanum](https://gitlab.gnome.org/World/Solanum).

## Glade not recommended recommended

At the end of last year, Christopher Davis published a blog post titled ["Glade
Not Recommended"](https://blogs.gnome.org/christopherdavis/2020/11/19/glade-not-recommended/).
Around that time, I was using Builder's UI designer to work on Sums' UI, but I
wasn't really satisfied. Even as a novice developer I was frustrated with the
way it chewed up my `GtkBuilder` files.

Davis' blog post gave me the confidence to commit to writing my UI files by
hand. I'd recommend this to any new developer: it's given me a much better 
understanding of how my application's UI fits together.
